import React, { Component } from "react";
import UserService from "./UserService";
export class AddUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      age: "",
    };
  }
  formValidation() {
    let nameError = "";
    let ageError = "";
    if (!this.state.username) nameError = "Name is required";
    if (!this.state.age) ageError = "Age is required";
    console.log(nameError);
    if (nameError || ageError) {
      this.setState({ nameError, ageError });

      return false;
    }

    return true;
  }
  submit = (e) => {
    e.preventDefault();
    if (this.formValidation()) {
      let user = { username: this.state.username, age: this.state.age };
      UserService.addUser(user);
      window.location.replace("/");
      this.setState({ nameError: "", ageError: "", username: "", age: "" });
    }
  };
  render() {
    return (
      <div>
        <form onSubmit={this.submit}>
          Username:
          <input
            type="text"
            name="username"
            value={this.state.username}
            onChange={(e) => this.setState({ username: e.target.value })}
          />
          <span className="text-danger">{this.state.nameError}</span>
          <br />
          Age:
          <input
            type="text"
            name="age"
            value={this.state.age}
            onChange={(e) => this.setState({ age: e.target.value })}
          />
          <span className="text-danger">{this.state.ageError}</span>
          <br />
          <button type="submit">Submit</button>
        </form>
      </div>
    );
  }
}

export default AddUser;
