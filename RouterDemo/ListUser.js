import React, { Component } from "react";
import UserService from "./UserService";

export class ListUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
    };
  }
  componentDidMount() {
    UserService.getAllUsers().then((result) => {
      this.setState({ users: result.data });
    });
  }
  render() {
    return (
      <div>
        <table className="table table-stripped table-bordered">
          <thead>
            <th>id</th>
            <th>Username</th>
            <th>age</th>
          </thead>
          <tbody>
            {
                this.state.users.map(user=>
                    <tr>
                        <td>{user.id}</td>
                        <td>{user.username}</td>
                        <td>{user.age}</td>
                    </tr>
                    )
            }
          </tbody>
        </table>
      </div>
    );
  }
}

export default ListUser;
