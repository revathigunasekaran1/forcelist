import axios from "axios";
let url = "http://localhost:7070";
class UserService {
  addUser(user) {
    axios.post(`${url}/addUser`, user);
    
  }
  getAllUsers()
  {
   return axios.get(`${url}/listUsers`)
  }
}
export default new UserService();
