import { useState } from "react";
import UserService from "./UserService";

function FAddUser() {
  //let [username,setusername]=useState('');
  //let [age,setage]=useState('');
  let [user, setuser] = useState({ username: "", age: "" });
  let submit = (e) => {
    e.preventDefault();
    UserService.addUser(user);
  };
  let handleChange = (e) => {
    setuser({ ...user, [e.target.name]: e.target.value });
  };
  return (
    <form onSubmit={submit}>
      Username: <input type="text" name="username" onChange={handleChange} />
      <br />
      Age: <input type="text" name="age" onChange={handleChange} />
      <br />
      <button type="submit">Click Me</button>
    </form>
  );
}

export default FAddUser;
