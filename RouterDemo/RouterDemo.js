import React from "react";
import { BrowserRouter as Router, Link, Route, Routes } from "react-router-dom";
import AddUser from "./AddUser";
import ListUser from "./ListUser";
function RouterDemo() {
  return (
    <Router>
      <ul>
        <li>
          <Link to="/add">AddUser</Link>
        </li>
        <li>
          <Link to="/">Listusers</Link>
        </li>
      </ul>
      <Routes>
        <Route path="/add" element={<AddUser/>}></Route>
        <Route path="/" element={<ListUser/>}></Route>
      </Routes>
    </Router>
  );
}

export default RouterDemo;
