package com.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.dao.StudentRepo;
import com.demo.exception.StudentNotFoundException;
import com.demo.model.Student;
import com.demo.service.IStudentService;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class StudentServiceImpl implements IStudentService {

	@Autowired
	private StudentRepo repo;

	@Override
	public Student saveStudent(Student st) {
		return repo.save(st);
	}

	@Override
	public List<Student> getAllStudents() {
		return repo.findAll();
	}

	@Override
	public Optional<Student> getStudentById(int id) throws StudentNotFoundException {
		Optional<Student> op = repo.findById(id);
		if (!op.isPresent())
			op.orElseThrow(() -> new StudentNotFoundException("Student not found"));
		return op;
	}

}
