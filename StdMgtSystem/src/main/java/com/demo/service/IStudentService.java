package com.demo.service;

import java.util.List;
import java.util.Optional;

import com.demo.exception.StudentNotFoundException;
import com.demo.model.Student;

public interface IStudentService {

	Student saveStudent(Student st);

	List<Student> getAllStudents();

	Optional<Student> getStudentById(int id) throws StudentNotFoundException;

}
