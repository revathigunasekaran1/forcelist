package com.demo.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.exception.StudentNotFoundException;
import com.demo.model.Student;
import com.demo.service.IStudentService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class StudentController {
	@Autowired
	private IStudentService service;

	List<Student> ls = new LinkedList<>();
	
	@GetMapping("/listOfStudents")
	public List<Student> listOfStudents() {
		List<Student> ls = service.getAllStudents();
		return ls;
	}

	// Optional[1,"revathi",23]
	@GetMapping("/findStudentById/{id}")
	public ResponseEntity<Student> getStudentById(@PathVariable("id") int id) throws StudentNotFoundException {
		Optional<Student> op = null;
		op = service.getStudentById(id);
		return new ResponseEntity<>(op.get(),HttpStatus.CREATED);
	}

	@PostMapping("/saveStudent")
	public Student saveStudent(@RequestBody Student st) {
		Student st1 = service.saveStudent(st);
		return st1;
	}
}
