import axios from "axios";
let url = "http://localhost:3001/user";
class JsonServerApi {
  addUser(user) {
    axios.post(`${url}`, user);
    
  }
  getAllUsers()
  {
   return axios.get(`${url}`)
  }
}
export default new JsonServerApi();
