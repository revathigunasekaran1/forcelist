import React, { useEffect, useRef,useState } from 'react'
const UseEffectDemo=()=>
{
    const [name,setName]=useState("");
  //  let [count,setCount]=useState(0);
  let count=useRef(0);
    useEffect(()=>{
        console.log(count.current);
        count.current=count.current+1;
    })
    return(
        <div>
            <input type="text" onChange={(e)=>setName(e.target.value)}/>
            <h1>Name:{name}</h1>
            <h1>Count:{count.current}</h1>
        </div>
    )
}
export default UseEffectDemo