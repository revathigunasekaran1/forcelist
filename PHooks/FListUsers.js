import { useEffect, useState } from "react";
import UserService from "./UserService";

function FListUsers() {
    const [users,setUsers]=useState([]);
  useEffect(() => {
    getUsers();
  }, []);
  const getUsers=async()=>
  {
  const response= await UserService.getAllUsers();
  console.log(response.data);
    setUsers(response.data);
    console.log(users)
  }

return(
<div>
    <table className="table table-stripped table-bordered">
          <thead>
            <th>id</th>
            <th>Username</th>
            <th>age</th>
          </thead>
          <tbody>
            {
                users.map(user=>
                    <tr>
                        <td>{user.id}</td>
                        <td>{user.username}</td>
                        <td>{user.age}</td>
                    </tr>
                    )
            }
          </tbody>
        </table>
      </div>
)
        }
export default FListUsers