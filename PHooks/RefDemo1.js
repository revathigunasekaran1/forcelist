import { useEffect, useRef,useState } from "react";

export default function RefDemo1() {
  // create a ref
  const counter = useRef(0);
  const [a, sa] = useState({});

  // increase the counter by one
  const handleIncreaseCounter = (e) => {
    
    
    console.log("clicked" );
    };

  useEffect(() => {
    counter.current = counter.current + 1;
    console.log("counter changed to: ", counter.current);
  }, [counter,a]);

  const handlchag = (e) =>{
    let value = e.target.value;
    // sa(prev=>{
    //     return {
    //         ...prev,
    //         value
    //     }
    // });
  }

  return (
    <div>
      <h1>Learn about useRef!</h1>
      <h2>Value: {counter.current}</h2>
      <input type="text" name={a} onChange={handlchag} />
      <button onClick={handleIncreaseCounter}>
Increase counter
    </button>
    </div>
  );
}