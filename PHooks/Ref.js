import React, {useRef, useState, useEffect} from 'react'



export default function Ref() {

  let [name, setName] = useState("");

  const count = useRef(0)




  useEffect(() => {

    console.log(count.current);

    count.current = count.current + 1;



  });




  return (

    <div>



        <input type="text" value={name} onChange={e => setName(e.target.value)}></input>

        <h1>Render Count: {count.current}</h1>



    </div>

  )

}