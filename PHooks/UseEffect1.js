import React, { useEffect, useRef,useState } from 'react'
const UseEffect1=()=>
{
  
  let [count,setCount]=useState(0);
  let [count1,setCount1]=useState(5);
 
    useEffect(()=>{
       document.title=`${count1} messages` 
    },[count1])
    return(
        <div>
             <h1>Count:{count}</h1>
           <button onClick={()=>setCount(count+1)}>Increment</button>
         
           <h1>Count:{count1}</h1>
           <button onClick={()=>setCount1(count1+10)}>Increment</button>
        </div>
    )
}
export default UseEffect1