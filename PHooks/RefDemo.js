import React, { Component } from "react";

export class RefDemo extends Component {
  constructor(props) {
    super(props);
    this.email = React.createRef();
    this.counter = React.createRef(0);
    this.state = {
      id: 0,
    };
  }

  dosome = (e) => {
    this.setState({
      id: Math.random().toString(),
    });
    this.counter.current = this.counter.current + 1;
  };

  submit = (e) => {
    e.preventDefault();
    // this.setState({
    //   id: Math.random().toString(),
    // });
    
    console.log(this.email.current.value);
  };
  render() {
    return (
      <>
        {/* {this.state.id} */}
        <hr />
        {this.counter.current}
        <form onSubmit={this.submit}>
          <input
            type="text"
            defaultValue="Email"
            ref={this.email}
            onChange={this.dosome}
          />
          <button type="submit">Click Me</button>
        </form>
      </>
    );
  }
}

export default RefDemo;
