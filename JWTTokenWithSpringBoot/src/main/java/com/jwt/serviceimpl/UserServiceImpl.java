package com.jwt.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.jwt.model.User;
import com.jwt.repo.UserRepository;
import com.jwt.service.IUserService;

@Service
public class UserServiceImpl implements IUserService{
	@Autowired
	UserRepository repo;
	@Autowired
	private BCryptPasswordEncoder encoder;
	public Integer saveUser(User user) {
		user.setPassword(encoder.encode(user.getPassword()));
		return repo.save(user).getId();
	}

}
