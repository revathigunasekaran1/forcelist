package com.jwt;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jwt.model.User;
import com.jwt.repo.UserRepository;


@SpringBootApplication
public class DemoApplication {
	@Autowired
	UserRepository repo;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

//	@PostConstruct
//	public void loadData() {
//		List<User> user = new LinkedList<User>();
//		Set<String> set = new LinkedHashSet<String>();
//		set.add("admin");
//		user.add(new User(2, "revathi", "revathi", "pass", set));
//		user.add(new User(3, "drish", "drishna", "pass123", set));
//		repo.saveAll(user);
//
//	}
}
