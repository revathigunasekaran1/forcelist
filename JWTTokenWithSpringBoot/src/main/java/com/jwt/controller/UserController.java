package com.jwt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.dto.UserRequest;
import com.jwt.model.User;
import com.jwt.service.IUserService;
import com.jwt.util.JWTUtil;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private IUserService service;

	@Autowired
	private AuthenticationManager authManager;
	@Autowired
	private JWTUtil util;
	@PostMapping("/save")
	public ResponseEntity<String> saveUser(@RequestBody User user) {
		System.out.println("save user");
		Integer id=service.saveUser(user);
		String body="User"+id+"saved";
		return ResponseEntity.ok(body);
		//return new ResponseEntity<>(body,HttpStatus.ok)
	}
	@PostMapping("/authenticate")
	public ResponseEntity<String> loginUser(@RequestBody UserRequest request)
	{
		System.out.println("Entered");
	authManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));	
		String token=util.generateToken(request.getUsername());
		return ResponseEntity.ok(token);
	}
	@GetMapping("/welcome")
	public String welcome()
	{
		return "welcome";
	}
}
