package com.jwt.xception;

public class UserNotFoundException extends Exception {
	public UserNotFoundException(String message)
	{
		super(message);
	}
}
