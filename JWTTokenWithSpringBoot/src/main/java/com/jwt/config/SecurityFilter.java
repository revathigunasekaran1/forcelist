package com.jwt.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.jwt.util.JWTUtil;

@Component
public class SecurityFilter extends OncePerRequestFilter {

	@Autowired
	private JWTUtil util;

	@Autowired
	private CustomerDetailsService service;

	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		String token = request.getHeader("Authorization");
		
		if (token != null) {
			String username = util.getUsername(token);
		
			if (username != null) {
				UserDetails usr = service.loadUserByUsername(username);
				boolean isValid=util.validateToken(token,usr.getUsername());
				if(isValid)
				{
					UsernamePasswordAuthenticationToken auth=new UsernamePasswordAuthenticationToken(usr.getUsername(), usr.getPassword(),usr.getAuthorities());
					auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));///user/welcome
					SecurityContextHolder.getContext().setAuthentication(auth);
					
				}

			}
		}
		filterChain.doFilter(request, response);
	}
	

}
