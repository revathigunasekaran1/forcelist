package com.jwt.config;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.jwt.model.User;
import com.jwt.repo.UserRepository;
import com.jwt.xception.UserNotFoundException;

@Service
public class CustomerDetailsService  implements UserDetailsService{

	@Autowired
	UserRepository repo;
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user=null;
		try
		{
		Optional<User> op=repo.findByUsername(username);
		user=op.get();
		if(op.isEmpty())
			throw new UserNotFoundException("User does not exist");
		}
		
		catch(UserNotFoundException ex)
		{
			System.out.println(ex);
		}
		return new org.springframework.security.core.userdetails
				.User(user.getUsername(),user.getPassword(),user.getRoles().stream().map(role->new SimpleGrantedAuthority(role)).collect(Collectors.toList()));
	}

}//revathi revathi  [admin,user]
