package com.jwt.util;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTUtil {

	@Value("${secret}")
	private String secret;

	
//	public String generateToken(String subject) {
//		Map<String,Object> claims=new HashMap<>();
//		return generateToken(claims,subject);
//
//	}
	public String generateToken(String subject)
	{

		return Jwts.builder()
				.setSubject(subject)
				.setIssuer("Virtusa")
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(1)))
				.signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encode(secret.getBytes()))
				.compact();
	}

	public Claims getClaims(String token) {

		return Jwts.parser().setSigningKey(Base64.getEncoder().encode(secret.getBytes())).parseClaimsJws(token).getBody();
	}

	public String getUsername(String token) {
	
		return getClaims(token).getSubject();
	}

	public boolean validateToken(String token, String username) {
		String usr = getUsername(token);
		return (username.equals(usr) && !isTokenExpired(token));
	}

	private boolean isTokenExpired(String token) {
	Date expDate=	getExpiredDate(token);
		return expDate.before(new Date());
	}

	private Date getExpiredDate(String token) {
		
		return getClaims(token).getExpiration();
	}
	
}
