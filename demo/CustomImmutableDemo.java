package com.demo;

public class CustomImmutableDemo {
private  int id;
private  String name;
protected void display()
{
	System.out.println("protected method");
}
public CustomImmutableDemo(int id, String name) {
	this.id=id;
	this.name=name;
}
public int getId() {
	return id;
}
public String getName() {
	return name;
}
public void setId(int id) {
	throw new UnsupportedOperationException();
}
public void setName(String name) {
	throw new UnsupportedOperationException();
}




}
