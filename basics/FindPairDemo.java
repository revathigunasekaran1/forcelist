package com.basics;

public class FindPairDemo {

	static void findPairWithCloseToZero(int arr[])// 1,3,-5,7,8,20,-40,6
	{
		int firstIndex = 0;
		int secondIndex = 1;
		int minSum = arr[0] + arr[1];
		for (int i = 0; i < arr.length; i++)// 1
		{
			for (int j = i + 1; j < arr.length; j++) {
				int sum = arr[i] + arr[j];
				if (Math.abs(sum) <Math.abs(minSum)) {//4<4
					firstIndex = i;
					secondIndex = j;
					minSum = sum;
				}
			}
		}
		System.out.println(arr[firstIndex] + " " + arr[secondIndex]);
	}

	public static void main(String[] args) {
		int a[] = { 1, 3, -5, 7, 8, 20, -40, 6 };
		findPairWithCloseToZero(a);
		//System.out.println(Math.abs(45));
	}
}