package com.basics;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class MapDemo {
public static void main(String[] args) {
	Map<String,Integer> map=new TreeMap<>();
	map.put("maths",40);//
	map.put("chemistry",300);
	map.put("physics",500);
	System.out.println(map);
	Iterator itr=map.entrySet().iterator();
	while(itr.hasNext())
	{
		System.out.println(itr.next());
	}
		//map.forEach((k,v)->System.out.println(k+" "+v));
		//System.out.println(map);//{maths=40,chemistry=300,physics=500}
//	System.out.println(map);
//	System.out.println(map.keySet());//maths,ch,ph
//	System.out.println(map.values());//40,300,500
//	System.out.println(map.entrySet());//[chemistry=300,maths=40,physics=500]
	
}
}
