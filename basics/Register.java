package com.basics;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Register implements Serializable {
private String username;
private String password;
private transient String conPassword;

}
