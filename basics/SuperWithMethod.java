package com.basics;

class CAnimal {
	void eat() {
		System.out.println("Animal Eat");
	}
}

class Dog extends CAnimal {
	void eat() {
		super.eat();//Animal Eat
		System.out.println("Dog Eat");//Dog Eat
	}
}

public class SuperWithMethod {
	public static void main(String[] args) {
		Dog d = new Dog();
		d.eat();
	}
}
