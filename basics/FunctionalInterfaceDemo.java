package com.basics;

@FunctionalInterface
interface Square
{
	int area(int x);
	
	
}
public class FunctionalInterfaceDemo{
public static void main(String[] args) {
	Square s=(x)->{return x*x;};//16
	s.area(4);
}

}
