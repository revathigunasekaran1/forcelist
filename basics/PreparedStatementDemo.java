package com.basics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class PreparedStatementDemo {
public static void main(String[] args) {
	try
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/forcelist","root","root");
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter department");
		String department=sc.nextLine();
		System.out.println("Enter salary");
		int salary=Integer.parseInt(sc.nextLine());
		PreparedStatement ps=con.prepareStatement("insert into tbl_employee(department,salary) values(?,?)");
		ps.setString(1, department);
		ps.setInt(2, salary);
		ps.executeUpdate();
		System.out.println("Success");
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
}
}
