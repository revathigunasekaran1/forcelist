package com.basics;

class Shape
{
	void area()
	{
		System.out.println("Shape"); 
	}
}
class Rectangle extends Shape
{
	void area()
	{
		System.out.println("Rectangle");
		
	}
}
class Sqaure extends Shape
{
	void area()
	{
		System.out.println("Square");
	}
}
public class UpcastingDemo {
public static void main(String[] args) {
	Shape s;
	s=new Shape();
	s.area();//shape
	s=new Rectangle();
	s.area();//rect
	s=new Sqaure();
	s.area();//square
	
	
}
}
