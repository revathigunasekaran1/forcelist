package com.basics;

//Super
class CEmployee {
	CEmployee() {
		System.out.println("Employee");// employee
	}

	CEmployee(int a) {
		System.out.println("Employee" + a);// employee
	}
}

class Programmer extends CEmployee {
	Programmer() {
		super();//parent class constaructor
		System.out.println("Programmer");// programmer
	}
}

public class SuperDemo {
	public static void main(String[] args) {
		Programmer p = new Programmer();
	}
}
