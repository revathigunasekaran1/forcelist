package com.basics;

public class SetCharAtDemo {
public static void main(String[] args) {
	String s1=new String("spring");//spring

	s1.concat("fall");//spring fall

	//s1="spring"
	s1=s1+"winter";//spring winter

	String s2=s1.concat("summer");//spring winter summer

	System.out.println(s1);//spring winter

	System.out.println(s2);//spring winter summer
}
}
