package com.basics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class LinkedListDemo {
	public static void main(String[] args) {
		LinkedList<String> ls = new LinkedList<>();
		List<CEmp> list = new ArrayList<>();// upcasting
		list.add(new CEmp(11, "revathi", 44566));
		list.add(new CEmp(2, "drish", 6789));
		list.add(new CEmp(3, "sheetal", 3214));
		Collections.sort(list);// 2 drish 6789 , 11 revathi 44566 ,3 sheetal 3214
		list.stream().filter(emp -> emp.getName().length() == 7)// 11 revathi 44566
				.map(p -> new CEmp(p.getId(), p.getName().toUpperCase(), p.getMob()))
				.forEach(emp -> System.out.println(emp));

	}
}
