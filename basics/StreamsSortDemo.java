package com.basics;

import java.util.LinkedList;
import java.util.List;

public class StreamsSortDemo {
public static void main(String[] args) {
	List<CEmp> ls=new LinkedList<>();
	ls.add(new CEmp(1,"revathi",45));
	ls.add(new CEmp(2,"drish",23));
	ls.add(new CEmp(3,"sheetal",29));
	ls.stream().sorted((e1,e2)->e1.getAge()-e2.getAge())
		.forEach(emp->System.out.println(emp));
}
}
