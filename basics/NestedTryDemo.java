package com.basics;

public class NestedTryDemo {
	public static void main(String args[]) {
		// Parent try block
		try {
			// child1 try block
			try {
				System.out.println("Inside 1st child block");
				int b = 23 / 0;
				System.out.println(b);
			} catch (ArrayIndexOutOfBoundsException ex) {
				System.out.println("ArrayIndex Exception");
			}
			// child2 try block
			try {
				System.out.println("Inside 2nd child block");
				int b = 23 / 0;
				System.out.println(b);

			} catch (ArithmeticException ex) {
				System.out.println("Child2 Arithmetic Exception");
			}
		} catch (ArithmeticException ex) {

			System.out.println("Parent Arithmetic Exception");
		}
	}
}
