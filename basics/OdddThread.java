package com.basics;

public class OdddThread extends Thread{
	FindOddOrEvenSync findOddOrEvenSync;
	public OdddThread(FindOddOrEvenSync findOddOrEvenSync)
	{
		this.findOddOrEvenSync=findOddOrEvenSync;
	}
	@Override
	public void run()
	{
		findOddOrEvenSync.odd();
	}
}
