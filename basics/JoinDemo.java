package com.basics;
class EvenThread extends Thread
{
	@Override
	public void run()
	{
		for(int i=0;i<10;i++)
		{
			if(i%2==0)
			{ try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				System.out.println(i);
			}
		}
	}
}
class OddThread extends Thread
{
	@Override
	public void run()
	{
		for(int i=0;i<10;i++)
		{
			if(i%2!=0)
			{
				System.out.println(i);
			}
		}
	}
}
public class JoinDemo {
public static void main(String[] args) throws InterruptedException {
	
	EvenThread et=new EvenThread();
	et.start();
	et.join();
	OddThread ot=new OddThread();
	ot.start();
	ot.join();
	
	
	
}
}
