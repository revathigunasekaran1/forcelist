package com.basics;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueDemo {
public static void main(String[] args) {
	Queue<Integer> queue=new PriorityQueue<Integer>(Collections.reverseOrder());
	queue.add(400);
	queue.add(200);
	queue.add(600);
	queue.add(900);
	queue.add(700);	
	queue.forEach(no->System.out.println(no));
	
	
}
}
