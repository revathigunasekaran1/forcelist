package com.basics;

import java.util.LinkedHashSet;

public class LinkedHashSetWithObject {
public static void main(String[] args) {
	LinkedHashSet<CEmp> set=new LinkedHashSet<>();
	set.add(new CEmp(1001,null,8827));
	set.add(new CEmp("revathi",7875));
	set.add(new CEmp(1003,"revathi",6476));
	//System.out.println(set);
	set.forEach(CEmp->System.out.println(CEmp.getId()));
	
	
	
}
}
