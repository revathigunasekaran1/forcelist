package com.basics;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationDemo {
public static void main(String[] args) {
	try
	{
	//CEmp emp=new CEmp(1,"revathi",23);//SERIALIZATION
		Register rs=new Register("revathi","revathi123","revathi123");
	FileOutputStream out=new FileOutputStream("emp.txt");
	ObjectOutputStream oout=new ObjectOutputStream(out);
	oout.writeObject(rs);
	FileInputStream in=new FileInputStream("emp.txt");
	ObjectInputStream ois=new ObjectInputStream(in);
	Register obj=(Register)ois.readObject();//1 revthi 23
	System.out.println(obj.getUsername()+" "+obj.getPassword() +" "+obj.getConPassword());
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
}
}
