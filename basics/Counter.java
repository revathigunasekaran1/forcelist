package com.basics;

public class Counter {
	static int count = 5;

	Counter() {
		++count;//8
		System.out.println(count);//8
	}

	public static void main(String[] args) {
		Counter c1 = new Counter();//6
		Counter c2 = new Counter();//7
		Counter c3 = new Counter();//8
	}
}
