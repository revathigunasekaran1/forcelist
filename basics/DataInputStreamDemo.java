package com.basics;

import java.io.DataInputStream;
import java.io.IOException;

public class DataInputStreamDemo {
	void getDataFromUser() throws IOException {
		DataInputStream dis = new DataInputStream(System.in);
		dis.readLine();
	}
}
