package com.basics;

class InvalidAgeException extends Exception // Throwable
{
	InvalidAgeException(String message) {
		super(message);
	}

}

public class CustomException {
	static void validateAge(int age) {
		try
		{
		if (age < 18)
			throw new InvalidAgeException("U r not eligible to vote");
		else
			System.out.println("welcome to vote");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());//class name and exception message
		}
	}

	public static void main(String[] args) {
		validateAge(13);
	}
}
