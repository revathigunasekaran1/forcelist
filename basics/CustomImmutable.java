package com.basics;

final class ImmuEmployee {
	private final int id;
	private String name;

	public int getId() {
		return id;
	}

	public ImmuEmployee(int id,String name) {
		this.id=id;
		this.name = name;
	}
}

public class CustomImmutable {
	public static void main(String[] args) {
		ImmuEmployee emp=new ImmuEmployee(1,"revathi");
		System.out.println(emp.hashCode());
		emp=new ImmuEmployee(2,"drish");
		System.out.println(emp.hashCode());
		System.out.println(emp.getId());
	}

}
