package com.basics;

class VarargsDemo {
	public int sumNumber(int ...args) {//[1,2,5]
		int sum=0;
		for(int i:args)
		{
			sum=sum+i;//sum=1+2
		}
		return sum;

	}

	public static void main(String args[]) {
		
	//	int a... = new ...5;

	}
}
