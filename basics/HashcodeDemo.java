package com.basics;

class Employee {

	private String empName;

	private int empNo;

	public Employee() {
	}

	public Employee(String empName, int empNo) {
		this.empName = empName;

		this.empNo = empNo;

	}

	public String getEmpName() {

		return empName;

	}

	public void setEmpName(String empName) {

		this.empName = empName;

	}

	public int getEmpNo() {

		return empNo;

	}

	public void setEmpNo(int empNo) {

		this.empNo = empNo;

	}
//
//	public int hashCode() {
//
//		return 12010;
//	}

	@Override
	public boolean equals(Object obj) {
		System.out.println(obj.getClass().getSimpleName());
		System.out.println("Super " + super.hashCode());// 1694819250
		System.out.println("Super " + obj.hashCode());
		return super.hashCode() == obj.hashCode();// 1234==12010
	} // 1694819250==12010
}

public class HashcodeDemo {

	public static void main(String[] args) {

		StringBuffer str = new StringBuffer("revathi");
		StringBuffer str1 = new StringBuffer("revathi");
		System.out.println(str.hashCode() + " " + str1.hashCode());
	 Employee mustafa = new Employee("Mohammed Mustafa", 8119132);
	 Employee bhumika = new Employee("Mohammed Mustafa", 8119132);
		Employee bhumika1 = new Employee("Bhumika MG1", 9113211);
		 System.out.println(mustafa.hashCode());// 12010
		 System.out.println(bhumika.hashCode());// 12010
		 System.out.println(mustafa.equals(bhumika));// false
		// System.out.println(mustafa.equals(bhumika1));// false
	}
}
