package com.basics;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateDemo {
public static void main(String[] args) {
	DateTimeFormatter dtf=DateTimeFormatter.ofPattern("hh:mm:ss a");
	LocalDateTime ldt=LocalDateTime.now().plus(3,ChronoUnit.HOURS);
	System.out.println(dtf.format(ldt));//9-11-2022 12:17+ 5
	
	
//	LocalTime time=LocalTime.now();
//	DateTimeFormatter dtf=DateTimeFormatter.ofPattern("hh:mm:ss a");
//	Duration threeHours=Duration.ofHours(3);
//	LocalTime result=time.plus(threeHours);
//	System.out.println(dtf.format(result));
	
}
}
