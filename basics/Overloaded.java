package com.basics;

public class Overloaded {
    void add(int a, float b) {
        System.out.println("1st method");
    }
//    void add(float a,int b) {
//        System.out.println("2nd method");
//    }
public static void main(String[] args) {
    Overloaded o=new Overloaded();
    o.add(10, 10);
}
}