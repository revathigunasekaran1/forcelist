package com.basics;

import java.io.DataInputStream;
import java.io.IOException;

public class RuntimeInputDemo {
	void getDataFromUser() throws IOException {
		DataInputStream st = new DataInputStream(System.in);
		st.readLine();
	}
}
