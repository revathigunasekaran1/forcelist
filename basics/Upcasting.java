package com.basics;
class Car
{
	void start()
	{
		System.out.println("Car is started");
	}
	void run()
	{
		System.out.println("Car is running");
	}
}
class Audi extends Car
{
	void stop()
	{
		System.out.println("Car is stopped");
	}
	void run()
	{
		System.out.println("Car is running with 100km");
	}
}
public class Upcasting {
public static void main(String[] args) {
	Car car=new Audi();
	car.start();
	Audi audi=new Audi();//upcasting
	audi.stop();
	audi.start();
	audi.run();
}
}
