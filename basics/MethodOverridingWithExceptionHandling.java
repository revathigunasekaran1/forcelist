package com.basics;

class Super {
	void show() {
		System.out.println("Parent Show");
	}
}

class SubClass extends Super {

	@Override
	void show() throws ArithmeticException {
		System.out.println("Child show");
	}

}

public class MethodOverridingWithExceptionHandling {
	public static void main(String[] args) {
		SubClass s = new SubClass();
		s.show();
	}
}