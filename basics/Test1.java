package com.basics;

class Test1 {
	public static void main(String[] args) throws InterruptedException {
		
		printAll(args);
	}

	public static void printAll(String[] lines) throws InterruptedException {
		for (int i = 0; i < lines.length; i++) {
			System.out.println(lines[i]);
			Thread.currentThread().sleep(1000);
		}
	}
}