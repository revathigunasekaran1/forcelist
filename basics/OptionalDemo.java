package com.basics;

import java.util.Optional;
import java.util.Scanner;

public class OptionalDemo {
	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(System.in);
			int empId = Integer.parseInt(sc.nextLine());
			CEmp emp = new CEmp(1, "revathi", 34);
			Optional<CEmp> op = Optional.of(emp);
			if (op.isPresent()&& op.get().getId()==empId) {
				System.out.println(op.get());
			}
			op=op.empty();
			op.orElseThrow(() -> new EmployeeIdNotFoundException("Employee Not Found"));
		} catch (EmployeeIdNotFoundException ex) {
			System.out.println(ex.getMessage());
		}
	}

}
