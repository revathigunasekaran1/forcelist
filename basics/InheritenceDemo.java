package com.basics;

class Animal1 {
	public String noise() {
		return "peep";
	}
}

class Dog1 extends Animal1 {
	@Override
	public String noise() {
		return "bark";
	}
}

class Cat extends Animal1 {
	@Override
	public String noise() {
		return "meow";
	}
}

class InheritenceDemo {

	public static void main(String[] args) {

	Animal1 animal = new Dog1();
		System.out.println(animal.noise());
		Dog1 cat = (Dog1) animal;
		System.out.println(cat.noise());
	}
}