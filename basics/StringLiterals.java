package com.basics;

public class StringLiterals {
public static void main(String[] args) {
	String s1=new String("bhaskar");//s1=bhaskar
	String s2=s1.concat("software");//s2=bhaskarsoftware
	String s3=s2.intern();//s3=bhaskarsoftware
	String s4="bhaskarsoftware";
	System.out.println(s2==s3);//true
	System.out.println(s3==s4);//true

}
}
