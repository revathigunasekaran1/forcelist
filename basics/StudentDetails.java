package com.basics;

class StudentDetails {
	int id;
	String name;
	StudentDetails() {
		System.out.println("Default Const");
	}
	StudentDetails(int id, String name) {
		this.id=id;//id=1
		this.name=name;//name=revathi
	}
	void display() {
		System.out.println(id + " " + name);
	}

	public static void main(String args[]) {
		StudentDetails st = new StudentDetails();
		StudentDetails st1 = new StudentDetails(1, "revathi");
		st1.display();
	}
}
