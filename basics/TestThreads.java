package com.basics;

class MyThread extends Thread {
	MyThread() {
		System.out.print(" MyThread");
	}

	@Override
	public void run() {
		System.out.print(" bar");
	}

	
}

public class TestThreads {
	public static void main(String[] args) {
		Thread t = new MyThread() {
			@Override
			public void run() {
				System.out.print(" foo");
			}
		};
		t.start();
	}
}
