package com.basics;

interface A2 {
	void x();
	void y();
}

class B2 implements A2 {
	@Override
	public void x() {
	}

	@Override
	public void y() {
	}
}

class C2 extends B2 {
	@Override
	public void x() {
	}
}

public class SCJP6 {
	public static void main(String[] args) {

		java.util.List<A2> list = new java.util.ArrayList<A2>();
		list.add(new B2());
		list.add(new C2());
		for (A2 a : list) {
			a.x();
			a.y();
		}
	}
}