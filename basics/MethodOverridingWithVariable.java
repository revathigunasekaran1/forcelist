package com.basics;

class Parent{
    int x;
    void display(){
        System.out.println("display method of parent class");
    }
    
    void OnlyParentDisplay(){
        System.out.println("OnlyParentDisplay method of Parent");
    }
}
  
class Child extends Parent{
  int x;
//    void display(){
//        System.out.println("display method of Child class");
//    }
    void OnlyChildDisplay(){
        System.out.println("OnlyParentDisplay method of Child");
    }
}
  
public class MethodOverridingWithVariable {
    public static void main(String[] args) {
        Parent p = new Child();
        p.display();//display method of Child class
        p.OnlyParentDisplay();//OnlyParentDisplay method of Parent
        System.out.println(p.x);//20
    }
}
