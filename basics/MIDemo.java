package com.basics;

interface Rabbit {

	void eat();
}

interface Tiger {
	void eat();
}

class Animal implements Rabbit, Tiger {
	public void eat() {
		System.out.println("Rabbit is eating");
	}

}

public class MIDemo {
	public static void main(String[] args) {
		Animal ani = new Animal();
		ani.eat();

	}
}
