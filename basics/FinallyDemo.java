package com.basics;

public class FinallyDemo {
	public static void main(String[] args) {
		try {
			int i = 1 / 1;
			System.out.println(i);
		}
		catch (ArithmeticException e) {
			System.out.println("Cannot divide by zero");
		}
		finally
		{
			System.out.println("Finally gonna run always");
		}

	}
}
