package com.basics;

import java.util.HashSet;

public class Group extends HashSet {
	public static void main(String[] args) {
		Group g = new Group();
		g.add(new Person("Hans"));
		g.add(new Person("Lotte"));
		g.add(new Person("Jane"));
		g.add(new Person("Hans"));
		g.add(new Person("Jane"));
		System.out.println("Total: " + g.size());
	}
	@Override
	public boolean add(Object o) {
		System.out.println("Adding: " + o);
		return super.add(o);
	}
}

