package com.basics;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class BinarySerachDemo {
	public static void search(List<String> list) {
		 list.clear();
		 list.add("b");//0
		 list.add("y");//-1 -2 
		 list.add("c");//b,y,c,h,i
		 list.add("h");//b,c,h,i,y
		 Collections.sort(list);//a-0,b-1,c-2
		 System.out.println(Collections.binarySearch(list, "i"));
		 }
public static void main(String[] args) {
	List<String> ls=new LinkedList<>();
	search(ls);
}
}
