package com.basics;

interface A1 { public void aMethod(); }
interface B1 { public void bMethod(); }
interface C extends A1,B1{ public void cMethod(); }
class D implements B1 {
@Override
public void bMethod(){
	System.out.println("From D");
}
}
class E extends D implements C {
@Override
public void aMethod(){}
@Override
public void bMethod(){
	System.out.println("From E");
}
@Override
public void cMethod(){}
public static void main(String[] args) {
//	D e=new E();
//	e.bMethod();
	D e = (new E());
			e.bMethod();
}
}

