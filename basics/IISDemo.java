package com.basics;

class IIS
{
	static int num;//1
	static{
		num=30;//2
	}
	IIS()//5
	{
		System.out.println("Inside constructor"+num);
	}
	void IISDisplay()//7
	{
		System.out.println("Inside Method");
	}
}
public class IISDemo {
public static void main(String[] args) {//3
	IIS iis=new IIS();//4
	iis.IISDisplay();//6
}
}
//static var
//static block
//main
//create instance
//default constructor
//method will be invoked
