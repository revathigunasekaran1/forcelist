package com.basics;

public class ExceptionDemo {
	static void test() {
		try {
			System.out.print("test ");
			throw new RuntimeException();
		} finally {
			System.out.print("exception ");
		}
	}

	public static void main(String[] args) {
		try {
			test();
		} finally {
			System.out.print("runtime ");
		}
		System.out.print("end ");
	}
}
