package com.basics;

abstract class Bank {
	abstract double getInterest();
	public double calculateEMI(double rate) {
		return 100000*rate;//
	}
}
class Sbi extends Bank {
	public double getInterest() {
		return 0.04;
	}
}
class Icici extends Bank {
	double getInterest() {
		return 0.09;
	}
}
class Hdfc extends Bank {
	double getInterest() {
		return 0.08;
	}
}
public class Banking {
	public static void main(String[] args) {
		Hdfc hdfc = new Hdfc();
		System.out.println(hdfc.getInterest());//0.08
		Sbi sbi = new Sbi();
		System.out.println(sbi.getInterest());//0.04
		Icici icici = new Icici();
		System.out.println(icici.getInterest());//0.09
		sbi.calculateEMI(sbi.getInterest());

	}
}