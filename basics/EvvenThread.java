package com.basics;

public class EvvenThread extends Thread {
	FindOddOrEvenSync findOddOrEvenSync;

	public EvvenThread(FindOddOrEvenSync findOddOrEvenSync) {
		this.findOddOrEvenSync = findOddOrEvenSync;
	}

	@Override
	public void run() {
		findOddOrEvenSync.even();
	}
}
