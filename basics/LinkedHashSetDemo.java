package com.basics;

import java.util.LinkedHashSet;

public class LinkedHashSetDemo {
	public static void main(String[] args) {
		LinkedHashSet<String> set = new LinkedHashSet<>();
		set.add("C");//insertion order
		set.add("C++");
		set.add("Java");
		set.add("Python");
		set.forEach(lang->System.out.println((lang)));
//		Iterator itr=set.iterator();
//		while(itr.hasNext())
//		{
//			System.out.println(itr.next());
//		}
//		
		//System.out.println(set);
	}
}
