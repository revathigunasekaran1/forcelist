package com.basics;

class FindOddOrEvenSync {
	public synchronized void odd() {
		for (int i = 0; i < 10; i++) {
			if (i % 2 != 0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(i);
			}
		}
	}
	public synchronized void even() {
		for (int i = 0; i < 10; i++) {
			if (i % 2 == 0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(i);
			}
		}
	}
}

