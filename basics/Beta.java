package com.basics;

class Alpha {
	public void foo(String... args) {
		System.out.println("Alpha:Foo");
	}

	public void bar(String... args) {
		System.out.println("Alpha:Bar");
	}
}

class Beta extends Alpha {
	@Override
	public void foo(String... args) {
		System.out.println("Beta:Foo");
	}

	@Override
	public void bar(String... args) {
		System.out.println("Beta:Bar");
	}
	public static void main(String args[]) {
		Alpha a = new Beta();//-->upcating
		Beta b=(Beta)(new Alpha());//-->downcasting
		a.foo("test");
		b.foo("test");
		a.bar("test");
		b.bar("test");
	}
}
