package com.basics;

public class MultiCatchDemo {
	public static void main(String[] args) {
		try {

			int i = 11 / 0;// 1
			System.out.println(i);// 1
		}
		catch (ArithmeticException e) {
			System.out.println("Cannot divide by zero");// 1
		}
		System.out.println("Rest of code");
	}
}
