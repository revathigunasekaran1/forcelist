package com.basics;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class MethodReferDemo {
public static void main(String[] args) {
	StringBuffer buf=new StringBuffer("Revathi");
	//defining a List  
	List<String> city = Arrays.asList("Boston", "San Diego", "Las Vegas", "Houston", "Miami", "Austin");  
	//iterate List using the ListIterator  
	ListIterator<String> listIterator = city.listIterator();  
	while(listIterator.hasPrevious())
	{  
	//prints the elements of the List  
	System.out.println(listIterator.previous()); 
	}  
	}  
}
}
