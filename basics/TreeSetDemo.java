package com.basics;

import java.util.TreeSet;

public class TreeSetDemo {
public static void main(String[] args) {//red-black tree
	TreeSet<CEmp> set=new TreeSet<>();//print the data in ascending order
	set.add(new CEmp(8,"rev",786889));
	set.add(new CEmp(1,"drish",4558));
	set.add(new CEmp(2,"sheela",6878));
	set.add(new CEmp(5,"meena",6878));
	set.add(new CEmp(9,"sheetal",6878));
	System.out.println(set);//34,56,130
}
}//Revathi,drish,revathi
