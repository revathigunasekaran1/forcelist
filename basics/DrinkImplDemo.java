package com.basics;

import java.util.TreeSet;

public class DrinkImplDemo {
	public static void main(String[] args) {
		Drink one = new Drink();
		Drink two = new Drink();
		Drink three = new Drink();
		one.name = "coffee";//89
		two.name = "tea";//78-89//10 20 5
		three.name = "boost";//78-89//5 20 10
		TreeSet set = new TreeSet();
		set.add(one);
		set.add(two);
		set.add(three);
		System.out.println(set);//10-20=-10
	}
}
