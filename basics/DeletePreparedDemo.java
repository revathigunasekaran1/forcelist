package com.basics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class DeletePreparedDemo {
public static void main(String[] args) {
	try
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/forcelist","root","root");
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Query");
		String str=sc.nextLine();
		PreparedStatement ps=con.prepareStatement(str);
		ResultSet rs=ps.executeQuery();
		System.out.printf("|%-8s |%10s %12s", "EMP_ID","DEPARTMENT","SALARY");
		System.out.println();
		
		while(rs.next())
		{ 	
			
			System.out.format("%-10s  %-8s %10s %n", rs.getInt(1),rs.getString(2),rs.getInt(3));
		}
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
}
}
//PreparedStatement ps=con.prepareStatement("delete from tbl_employee where emp_id=?");
//ps.setInt(1, 4);
//ps.executeUpdate();
//System.out.println("Success");