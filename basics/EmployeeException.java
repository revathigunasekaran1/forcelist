package com.basics;

class EmployeeIdNotFoundException extends RuntimeException {
	EmployeeIdNotFoundException(String message) {
		super(message);
	}
}

class InvalidEmployeeNameLengthException extends RuntimeException {
	InvalidEmployeeNameLengthException(String message) {
		super(message);
	}
}

class InvalidMobileNumberException extends Exception {
	InvalidMobileNumberException(String message) {
		super(message);
	}
}

public class EmployeeException {
	static void getEmployee(CEmp emp) {
		try {
			if (emp.getId() < 1 || emp.getId() > 999) {
				throw new EmployeeIdNotFoundException("Invalid Id");
			} else if (emp.getName().length() < 6)
				throw new InvalidEmployeeNameLengthException("Employee name should length >6");
			else if (String.valueOf(emp.getMob()).length() != 10) {// Long l=emp.getMob(); String mob=String.valueof(l);
				throw new InvalidMobileNumberException("Mobile number should contain 10 digits");
			} else {
				System.out.println("Employee Found");
			}
		} catch (EmployeeIdNotFoundException e) {

			System.out.println(e);
		} catch (InvalidEmployeeNameLengthException e) {
			System.out.println(e);
		} catch (InvalidMobileNumberException e) {
			System.out.println(e);
		}
	}

	public static void main(String[] args) {
		CEmp emp = new CEmp(1, "revathi", 88707);
		getEmployee(emp);
	}
}
