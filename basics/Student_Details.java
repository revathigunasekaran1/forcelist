package com.basics;

public class Student_Details{
private int id;
private String name;
private Address_Details address;//Has-A
public Student_Details(int id, String name,Address_Details address) {
	
	this.id = id;
	this.name = name;
	this.address=address;
}

public String toString() {
	return "Student_Details [id=" + id + ", name=" + name + ", address=" + address + "]";
}
}
