package com.basics;

class Test {
	int i;

	Test(int i) {
		this.i = i;
	}


	public static void main(String[] args) {
		Test t1 = new Test(10);
		//Test t2 = new Test(100);
		System.out.println(t1.toString());//com.basics.Test@hashcode//75A1CD57
		System.out.println(t1.hashCode());//1973538135
		//System.out.println(t2);
	}
}
