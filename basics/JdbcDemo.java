package com.basics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class JdbcDemo {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		try
		{
		Class.forName("com.mysql.cj.jdbc.Driver");
		// DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/forcelist", "root", "root");
		con.setAutoCommit(false);
		Statement st = con.createStatement();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Email");
		String email=sc.nextLine();
		st.executeUpdate("insert into subscribers(email) values('"+email+"')");
		con.commit();
		System.out.println("Success");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
}
