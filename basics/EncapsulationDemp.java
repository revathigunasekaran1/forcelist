package com.basics;

class Emp {
	private int id;
	private String name;
	Emp(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public String toString() {
		return "id=" + id + ", name=" + name + "";
	}
}
public class EncapsulationDemp {
	public static void main(String[] args) {
		Emp emp = new Emp(1, "revathi");
		System.out.println(emp);
	}
}
