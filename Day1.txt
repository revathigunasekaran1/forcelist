DDL(structurE)
DML(
DQL OR DRL
DCL
TCL


DDL
------
create
alter
---
   add   after&first
   drop
   modify
   rename to
   change column
   
drop


DML
----
insert 
delete
update
truncate 


delete                           truncate
delete from employee               truncate table employee
where                            where not possible
will not change the structure    will change the structure 
rollback                         cannot rollback

DQL OR DRL
--------

DDL Commands
--------

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| bankapp            |
| data1              |
| demo               |
| demo_database      |
| demodatabase       |
| hibernatedemo      |
| information_schema |
| jpm                |
| jpmc               |
| mydb               |
| mysql              |
| nodejs             |
| paymentdetails     |
| performance_schema |
| sys                |
| tariffmanager1     |
| testdatabase       |
| users_database     |
| usfs               |
| usk                |
+--------------------+
20 rows in set (0.07 sec)

mysql> create database forcelist;
Query OK, 1 row affected (0.05 sec)

mysql> use forcelist;
Database changed
mysql> drop database forcelist;
Query OK, 0 rows affected (0.11 sec)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| bankapp            |
| data1              |
| demo               |
| demo_database      |
| demodatabase       |
| hibernatedemo      |
| information_schema |
| jpm                |
| jpmc               |
| mydb               |
| mysql              |
| nodejs             |
| paymentdetails     |
| performance_schema |
| sys                |
| tariffmanager1     |
| testdatabase       |
| users_database     |
| usfs               |
| usk                |
+--------------------+
20 rows in set (0.00 sec)

mysql> create database forcelist;
Query OK, 1 row affected (0.01 sec)

mysql> use forcelist;
Database changed
mysql> create table emp(id int,name varchar(40));
Query OK, 0 rows affected (0.07 sec)

mysql> desc forcelist;
ERROR 1146 (42S02): Table 'forcelist.forcelist' doesn't exist
mysql> desc emp;
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| id    | int         | YES  |     | NULL    |       |
| name  | varchar(40) | YES  |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
2 rows in set (0.02 sec)

mysql> alter table emp add age int;
Query OK, 0 rows affected (0.06 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe emp;
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| id    | int         | YES  |     | NULL    |       |
| name  | varchar(40) | YES  |     | NULL    |       |
| age   | int         | YES  |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)

mysql> alter table emp add email varchar(40) after name;
Query OK, 0 rows affected (0.09 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe emp;
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| id    | int         | YES  |     | NULL    |       |
| name  | varchar(40) | YES  |     | NULL    |       |
| email | varchar(40) | YES  |     | NULL    |       |
| age   | int         | YES  |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
4 rows in set (0.01 sec)

mysql> alter table emp add phoneno varchar(40) before name;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'before name' at line 1
mysql> alter table emp add empid varchar(40) first;
Query OK, 0 rows affected (0.05 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe emp;
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| empid | varchar(40) | YES  |     | NULL    |       |
| id    | int         | YES  |     | NULL    |       |
| name  | varchar(40) | YES  |     | NULL    |       |
| email | varchar(40) | YES  |     | NULL    |       |
| age   | int         | YES  |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
5 rows in set (0.01 sec)

mysql> alter table emp drop column id;
Query OK, 0 rows affected (0.07 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> alter table emp add address varchar(10);
Query OK, 0 rows affected (0.02 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe emp;
+---------+-------------+------+-----+---------+-------+
| Field   | Type        | Null | Key | Default | Extra |
+---------+-------------+------+-----+---------+-------+
| empid   | varchar(40) | YES  |     | NULL    |       |
| name    | varchar(40) | YES  |     | NULL    |       |
| email   | varchar(40) | YES  |     | NULL    |       |
| age     | int         | YES  |     | NULL    |       |
| address | varchar(10) | YES  |     | NULL    |       |
+---------+-------------+------+-----+---------+-------+
5 rows in set (0.01 sec)

mysql> alter table emp modify address varchar(50);
Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe emp;
+---------+-------------+------+-----+---------+-------+
| Field   | Type        | Null | Key | Default | Extra |
+---------+-------------+------+-----+---------+-------+
| empid   | varchar(40) | YES  |     | NULL    |       |
| name    | varchar(40) | YES  |     | NULL    |       |
| email   | varchar(40) | YES  |     | NULL    |       |
| age     | int         | YES  |     | NULL    |       |
| address | varchar(50) | YES  |     | NULL    |       |
+---------+-------------+------+-----+---------+-------+
5 rows in set (0.00 sec)

mysql> alter table emp change column name empname varchar(40);
Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe emp;
+---------+-------------+------+-----+---------+-------+
| Field   | Type        | Null | Key | Default | Extra |
+---------+-------------+------+-----+---------+-------+
| empid   | varchar(40) | YES  |     | NULL    |       |
| empname | varchar(40) | YES  |     | NULL    |       |
| email   | varchar(40) | YES  |     | NULL    |       |
| age     | int         | YES  |     | NULL    |       |
| address | varchar(50) | YES  |     | NULL    |       |
+---------+-------------+------+-----+---------+-------+
5 rows in set (0.01 sec)

mysql> alter table emp rename to employee;
Query OK, 0 rows affected (0.04 sec)

mysql> describe emp;
ERROR 1146 (42S02): Table 'forcelist.emp' doesn't exist
mysql> describe employee;
+---------+-------------+------+-----+---------+-------+
| Field   | Type        | Null | Key | Default | Extra |
+---------+-------------+------+-----+---------+-------+
| empid   | varchar(40) | YES  |     | NULL    |       |
| empname | varchar(40) | YES  |     | NULL    |       |
| email   | varchar(40) | YES  |     | NULL    |       |
| age     | int         | YES  |     | NULL    |       |
| address | varchar(50) | YES  |     | NULL    |       |
+---------+-------------+------+-----+---------+-------+
5 rows in set (0.01 sec)

mysql> create table student(id int);
Query OK, 0 rows affected (0.03 sec)

mysql> drop table student;
Query OK, 0 rows affected (0.04 sec)

mysql> create table student(id int);
Query OK, 0 rows affected (0.03 sec)

mysql> alter table student
    -> add name varchar(30)
    -> add age int;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'add age int' at line 3
mysql> alter table student
    -> add name varchar(30),
    -> add age int;
Query OK, 0 rows affected (0.04 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe student;
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| id    | int         | YES  |     | NULL    |       |
| name  | varchar(30) | YES  |     | NULL    |       |
| age   | int         | YES  |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)

mysql> show tables;
+---------------------+
| Tables_in_forcelist |
+---------------------+
| employee            |
+---------------------+
1 row in set (0.01 sec)

mysql> show tables from forcelist;
+---------------------+
| Tables_in_forcelist |
+---------------------+
| employee            |
+---------------------+
1 row in set (0.00 sec)

mysql> show tables in forcelist;
+---------------------+
| Tables_in_forcelist |
+---------------------+
| employee            |
+---------------------+
1 row in set (0.00 sec)

mysql> insert into employee(empid,empname)
    -> values
    -> (6,"sheetal");
Query OK, 1 row affected (0.01 sec)

mysql> select * from employee;
+-------+---------+-------------------+------+---------+
| empid | empname | email             | age  | address |
+-------+---------+-------------------+------+---------+
| 1     | revathi | revathi@gmail.com |   34 | chennai |
| 2     | drish   | drish@gmail.com   |   23 | chennai |
| 3     | sheela  | sheela@gmail.com  |   34 | chennai |
| 6     | sheetal | NULL              | NULL | NULL    |
+-------+---------+-------------------+------+---------+
4 rows in set (0.00 sec)

DML Commands
---------------

mysql> create table employee
    -> (empid int primary key auto_increment,
    -> empname varchar(30));
Query OK, 0 rows affected (0.06 sec)

mysql> desc employee;
+---------+-------------+------+-----+---------+----------------+
| Field   | Type        | Null | Key | Default | Extra          |
+---------+-------------+------+-----+---------+----------------+
| empid   | int         | NO   | PRI | NULL    | auto_increment |
| empname | varchar(30) | YES  |     | NULL    |                |
+---------+-------------+------+-----+---------+----------------+
2 rows in set (0.01 sec)


mysql> select * from employee;
+-------+---------+
| empid | empname |
+-------+---------+
|     1 | revathi |
|     2 | sheela  |
+-------+---------+
2 rows in set (0.00 sec)

mysql> insert into employee
    -> values
    -> (2,"sheetal");
ERROR 1062 (23000): Duplicate entry '2' for key 'employee.PRIMARY'
mysql> insert into employee
    -> values
    -> ("sheetal");
ERROR 1136 (21S01): Column count doesn't match value count at row 1
mysql> insert into employee(empname)
    -> values
    -> ("sheetal");
Query OK, 1 row affected (0.00 sec)


mysql> select * from employee;
+-------+---------+
| empid | empname |
+-------+---------+
|     1 | revathi |
|     2 | sheela  |
|     3 | sheetal |
+-------+---------+
3 rows in set (0.00 sec)


mysql> insert into employee
    -> values
    -> (6,"veena");
Query OK, 1 row affected (0.01 sec)

mysql> select * from employee;
+-------+---------+
| empid | empname |
+-------+---------+
|     1 | revathi |
|     2 | sheela  |
|     3 | sheetal |
|     6 | veena   |
+-------+---------+
4 rows in set (0.00 sec)

mysql> insert into employee
    -> values
    -> ("meetal");
ERROR 1136 (21S01): Column count doesn't match value count at row 1
mysql> insert into employee(empname)
    -> values
    -> ("meetal");
Query OK, 1 row affected (0.01 sec)

mysql> select * from employee;
+-------+---------+
| empid | empname |
+-------+---------+
|     1 | revathi |
|     2 | sheela  |
|     3 | sheetal |
|     6 | veena   |
|     7 | meetal  |
+-------+---------+
5 rows in set (0.00 sec)


mysql> select * from employee;
+-------+---------+
| empid | empname |
+-------+---------+
|     1 | revathi |
|     2 | sheela  |
|     3 | sheetal |
|     6 | veena   |
|     7 | meetal  |
+-------+---------+
5 rows in set (0.00 sec)

mysql> delete from employee;
Query OK, 5 rows affected (0.00 sec)

mysql> select * from employee;
Empty set (0.00 sec)

mysql> insert into employee(empname)
    -> values
    -> ("meetal");
Query OK, 1 row affected (0.01 sec)

mysql> select * from employee;
+-------+---------+
| empid | empname |
+-------+---------+
|     8 | meetal  |
+-------+---------+
1 row in set (0.00 sec)

mysql> truncate table employee;
Query OK, 0 rows affected (0.06 sec)

mysql> select * from employee;
Empty set (0.01 sec)

mysql> insert into employee(empname)
    -> values
    -> ("meetal");
Query OK, 1 row affected (0.00 sec)

mysql> select * from employee;
+-------+---------+
| empid | empname |
+-------+---------+
|     1 | meetal  |
+-------+---------+
1 row in set (0.00 sec)
mysql> update employee set empname='revathi';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from employee;
+-------+---------+
| empid | empname |
+-------+---------+
|     1 | revathi |
+-------+---------+
1 row in set (0.01 sec)

mysql> update employee set empname='sri' where empid=4;
Query OK, 0 rows affected (0.00 sec)
Rows matched: 0  Changed: 0  Warnings: 0

mysql> update employee set empname='sri' where empid=1;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> update employee set empname='alice',age=23 where empid=1;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from employee;
+-------+---------+------+
| empid | empname | age  |
+-------+---------+------+
|     1 | alice   |   23 |
+-------+---------+------+
1 row in set (0.00 sec)

DQL OR DRL
---------

mysql> select * from employee where empid=1;
+-------+---------+------+
| empid | empname | age  |
+-------+---------+------+
|     1 | alice   |   23 |
+-------+---------+------+
1 row in set (0.00 sec)

mysql> select empid from employee;
+-------+
| empid |
+-------+
|     1 |
+-------+
1 row in set (0.00 sec)

mysql> select empid,empname from employee;
+-------+---------+
| empid | empname |
+-------+---------+
|     1 | alice   |
+-------+---------+
1 row in set (0.00 sec)


mysql> select * from employee;
+-------+---------+------+
| empid | empname | age  |
+-------+---------+------+
|     1 | alice   |   23 |
|     2 | bob     |   34 |
|     3 | sree    |   45 |
+-------+---------+------+
3 rows in set (0.00 sec)

mysql> select * from employee order by age;
+-------+---------+------+
| empid | empname | age  |
+-------+---------+------+
|     1 | alice   |   23 |
|     2 | bob     |   34 |
|     3 | sree    |   45 |
+-------+---------+------+
3 rows in set (0.00 sec)

mysql> select * from employee order by age desc;
+-------+---------+------+
| empid | empname | age  |
+-------+---------+------+
|     3 | sree    |   45 |
|     2 | bob     |   34 |
|     1 | alice   |   23 |
+-------+---------+------+
3 rows in set (0.00 sec)


mysql> select * from employee order by 1 desc;
+-------+---------+------+
| empid | empname | age  |
+-------+---------+------+
|     3 | sree    |   45 |
|     2 | bob     |   34 |
|     1 | alice   |   23 |
+-------+---------+------+
3 rows in set (0.00 sec)

Aggregate function
---------
min()
max()
avg()
count()
sum()

mysql> select * from employee;
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     1 | alice   |   23 |   1000 |
|     2 | bob     |   34 |   2000 |
|     3 | sree    |   45 |   3000 |
+-------+---------+------+--------+
3 rows in set (0.00 sec)

mysql> select sum(salary) from employee;
+-------------+
| sum(salary) |
+-------------+
|        6000 |
+-------------+
1 row in set (0.00 sec)

mysql> select sum(salary) "sum of salary" from employee;
+---------------+
| sum of salary |
+---------------+
|          6000 |
+---------------+
1 row in set (0.00 sec)


mysql> select sum(salary) salary from employee;
+--------+
| salary |
+--------+
|   6000 |
+--------+
1 row in set (0.00 sec)

mysql> select sum(salary) as salary from employee;
+--------+
| salary |
+--------+
|   6000 |
+--------+
1 row in set (0.00 sec)


mysql> select * from employee;
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     1 | alice   |   23 |   1000 |
|     2 | bob     |   34 |   2000 |
|     3 | sree    |   45 |   3000 |
|     4 | meeval  |   23 |   NULL |
+-------+---------+------+--------+
4 rows in set (0.00 sec)

mysql> select count(salary) as salary from employee;
+--------+
| salary |
+--------+
|      3 |
+--------+
1 row in set (0.00 sec)

mysql> select count(*) as salary from employee;
+--------+
| salary |
+--------+
|      4 |
+--------+
1 row in set (0.02 sec)


mysql> select avg(salary) as avgsalary from employee;
+-----------+
| avgsalary |
+-----------+
| 2000.0000 |
+-----------+
1 row in set (0.00 sec)

mysql> select avg(ifnull(salary,0)) as avgsalary from employee;
+-----------+
| avgsalary |
+-----------+
| 1500.0000 |
+-----------+
1 row in set (0.00 sec)

mysql> select avg(coalesce(salary,0)) as avgsalary from employee;
+-----------+
| avgsalary |
+-----------+
| 1500.0000 |
+-----------+
1 row in set (0.00 sec)

mysql> select * from employee where salary=null;
Empty set (0.01 sec)

mysql> select * from employee where salary is null;
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     4 | meeval  |   23 |   NULL |
+-------+---------+------+--------+
1 row in set (0.00 sec)

mysql> select * from employee where salary between 2000 and 3000;
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     2 | bob     |   34 |   2000 |
|     3 | sree    |   45 |   3000 |
+-------+---------+------+--------+

mysql> select * from employee where salary not  between 2000 and 3000;
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     1 | alice   |   23 |   1000 |
+-------+---------+------+--------+
1 row in set (0.00 sec)

mysql> select * from employee
    -> where empname like '_o%';
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     2 | bob     |   34 |   2000 |
+-------+---------+------+--------+
1 row in set (0.00 sec)

mysql> select * from employee
    -> where empname like 'a%';
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     1 | alice   |   23 |   1000 |
+-------+---------+------+--------+
1 row in set (0.00 sec)

mysql> select * from employee
    -> where empname like '_lice';
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     1 | alice   |   23 |   1000 |
+-------+---------+------+--------+
1 row in set (0.00 sec)


mysql> select * from employee
    -> where empname='bob'
    -> and salary>4000;
Empty set (0.00 sec)

mysql> select * from employee;
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     1 | alice   |   23 |   1000 |
|     2 | bob     |   34 |   2000 |
|     3 | sree    |   45 |   3000 |
|     4 | meeval  |   23 |   NULL |
+-------+---------+------+--------+
4 rows in set (0.00 sec)

mysql> select * from employee
    -> where empname='bob'
    -> or salary>4000;
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     2 | bob     |   34 |   2000 |
+-------+---------+------+--------+
1 row in set (0.00 sec)

mysql>
mysql> select * from employee where empname <> 'bob';
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     1 | alice   |   23 |   1000 |
|     3 | sree    |   45 |   3000 |
|     4 | meeval  |   23 |   NULL |
+-------+---------+------+--------+
3 rows in set (0.00 sec)

mysql> select * from employee where empname != 'bob';
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     1 | alice   |   23 |   1000 |
|     3 | sree    |   45 |   3000 |
|     4 | meeval  |   23 |   NULL |
+-------+---------+------+--------+
3 rows in set (0.00 sec)

mysql> select * from employee where empname in('alice','sheela','bob');
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     1 | alice   |   23 |   1000 |
|     2 | bob     |   34 |   2000 |
+-------+---------+------+--------+
2 rows in set (0.00 sec)

mysql> select * from employee where empname not in('alice','sheela','bob');
+-------+---------+------+--------+
| empid | empname | age  | salary |
+-------+---------+------+--------+
|     3 | sree    |   45 |   3000 |
|     4 | meeval  |   23 |   NULL |
+-------+---------+------+--------+
2 rows in set (0.00 sec)

Empid   empaname  date         workinghours
1       revathi   9-11-2022      1
2       sheela    9-11-2022      34
3       revathi   9-11-2022        12
4      sheela     9-11-2022        23


select empname,sum(workinghours)
from employee
group by empname



revathi    1
revathi    12




sheela    34
sheela    23



revathi     13
sheela      57



mysql> select * from employee;
+-------+---------+------+--------+-----------+
| empid | empname | age  | salary | location  |
+-------+---------+------+--------+-----------+
|     1 | alice   |   23 |   1000 | chennai   |
|     2 | bob     |   34 |   2000 | noida     |
|     3 | sree    |   45 |   3000 | bangalore |
|     4 | meeval  |   23 |   NULL | hyderabad |
+-------+---------+------+--------+-----------+
4 rows in set (0.00 sec)

mysql> select empname ,sum(salary)
    -> from employee
    -> group by empname;
+---------+-------------+
| empname | sum(salary) |
+---------+-------------+
| alice   |        1000 |
| bob     |        2000 |
| sree    |        3000 |
| meeval  |        NULL |
+---------+-------------+
4 rows in set (0.01 sec)

mysql> update employee
    -> set empname='alice'
    -> where empid=3;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> update employee
    -> set empname='bob'
    -> where empid=4;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from employee;
+-------+---------+------+--------+-----------+
| empid | empname | age  | salary | location  |
+-------+---------+------+--------+-----------+
|     1 | alice   |   23 |   1000 | chennai   |
|     2 | bob     |   34 |   2000 | noida     |
|     3 | alice   |   45 |   3000 | bangalore |
|     4 | bob     |   23 |   NULL | hyderabad |
+-------+---------+------+--------+-----------+
4 rows in set (0.00 sec)

mysql> select empname ,sum(salary)
    -> from employee
    -> group by empname;
+---------+-------------+
| empname | sum(salary) |
+---------+-------------+
| alice   |        4000 |
| bob     |        2000 |
+---------+-------------+
2 rows in set (0.00 sec)

mysql> update employee
    -> set empname='sree'
    -> where empid=3;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> update employee
    -> set empname='meeval'
    -> where empid=4;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from employee;
+-------+---------+------+--------+-----------+
| empid | empname | age  | salary | location  |
+-------+---------+------+--------+-----------+
|     1 | alice   |   23 |   1000 | chennai   |
|     2 | bob     |   34 |   2000 | noida     |
|     3 | sree    |   45 |   3000 | bangalore |
|     4 | meeval  |   23 |   NULL | hyderabad |
+-------+---------+------+--------+-----------+
4 rows in set (0.00 sec)

mysql> update employee
    -> set location='chennai'
    -> where empid=3;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from employee;
+-------+---------+------+--------+-----------+
| empid | empname | age  | salary | location  |
+-------+---------+------+--------+-----------+
|     1 | alice   |   23 |   1000 | chennai   |
|     2 | bob     |   34 |   2000 | noida     |
|     3 | sree    |   45 |   3000 | chennai   |
|     4 | meeval  |   23 |   NULL | hyderabad |
+-------+---------+------+--------+-----------+
4 rows in set (0.00 sec)

mysql> select location,count(empname) as count
    -> from employee
    -> group by location;
+-----------+-------+
| location  | count |
+-----------+-------+
| chennai   |     2 |
| noida     |     1 |
| hyderabad |     1 |
+-----------+-------+
3 rows in set (0.00 sec)

mysql> select location,count(empname) as count
    -> from employee
    -> group by location
    -> having count(empname)>=2;
+----------+-------+
| location | count |
+----------+-------+
| chennai  |     2 |
+----------+-------+
1 row in set (0.01 sec)

mysql>
















