package com.bank.controller;

import java.util.LinkedList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bank.model.Employee;

@RestController
public class RegisterController {
	List<Employee> ls=new LinkedList<>();
	@PostMapping("/addEmployee")
	public Employee addEmployee(@RequestBody Employee emp)
	{
		ls.add(emp);
		return emp;
	}
	@GetMapping("/listEmployees")
	public  List<Employee> listEmployees()
	{
		
		return ls;
		
	}
	@RequestMapping(value="/lsEmployees",method = RequestMethod.GET)
	public  List<Employee> lsEmployees()
	{
		List<Employee> ls=new LinkedList<>();
		ls.add(new Employee(1,"drish",23));
		return ls;
		
	}
}
