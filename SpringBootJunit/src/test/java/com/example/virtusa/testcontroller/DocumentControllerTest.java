package com.example.virtusa.testcontroller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.virtusa.controller.DocumentController;
import com.example.virtusa.model.Document;
import com.example.virtusa.service.IDocumentService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class DocumentControllerTest {
	
	@Autowired
	MockMvc mockMvc;

	@InjectMocks
	private DocumentController con;
	
	@Mock
	private IDocumentService service;
	private Document document;
	@BeforeEach
	public void setUp()
	{
		mockMvc=MockMvcBuilders.standaloneSetup(con).build();
//document=new Document(4,"revathi",456,"ay56","tn220","chennai");
document=new Document(4,"revathi",456);
	}
	@Test
	
	void saveDocument() throws Exception
	{
		
		service.saveDocument(document);
		ObjectMapper mapper=new ObjectMapper();
		String jsonbody=mapper.writeValueAsString(document);
		this.mockMvc.perform(post("/user/document")
				.content(jsonbody)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
				
		
		
	}
	@Test
	void testSaveNotDone() throws Exception
	{
		
		service.saveDocument(document);
		ObjectMapper mapper=new ObjectMapper();
		String jsonbody=mapper.writeValueAsString(document);
		this.mockMvc.perform(post("/user/document")
				.content(jsonbody)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isInternalServerError());
				
		
		
	}
	@Test
	void getDocumentByName() throws Exception
	{
		
		Mockito.when(service.getUserDocumentByName("revathi")).thenReturn(List.of(document));
		this.mockMvc.perform(get("/user/document/revathi"))
				.andExpect(status().isOk());	
	}
}
