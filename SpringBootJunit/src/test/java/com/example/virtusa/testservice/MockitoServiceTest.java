package com.example.virtusa.testservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.virtusa.model.Document;
import com.example.virtusa.repository.DocumentRepository;
import com.example.virtusa.service.impl.DocumentServiceImpl;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class MockitoServiceTest {

	@InjectMocks
	private DocumentServiceImpl service;
	
	@Mock
	private DocumentRepository repo;
	
	private Document document;
	@BeforeEach
	public void setUp()
	{
		document=new Document(3,"revathi",456,"ay56","tn220","chennai");
	}
	@Test
	void saveDocument()
	{
		when(repo.save(document)).thenReturn(document);
		Document doc=service.saveDocument(document);
		assertEquals(doc,document);
		
	}
	@Test
	void getDocuments()
	{
		when(repo.findByName(document.getName())).thenReturn(List.of(document));
		List<Document> ls=service.getUserDocumentByName(document.getName());
		assertEquals(ls.size(),1);
	}
}
