package com.example.virtusa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Document {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private long aadhar;
	private String pan;
	private String address;
	private String license;
	public Document(int id,String name,long aadhae)
	{
		this.id=id;
		this.name=name;
		this.aadhar=aadhae;
	}
}
