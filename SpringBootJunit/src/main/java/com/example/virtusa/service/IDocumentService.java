package com.example.virtusa.service;

import java.util.List;

import com.example.virtusa.model.Document;

public interface IDocumentService {

	Document saveDocument(Document document);

	List<Document> getUserDocumentByName(String name);
	
	
	

}
