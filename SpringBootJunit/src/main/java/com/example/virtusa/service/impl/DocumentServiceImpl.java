package com.example.virtusa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.virtusa.model.Document;
import com.example.virtusa.repository.DocumentRepository;
import com.example.virtusa.service.IDocumentService;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class DocumentServiceImpl implements IDocumentService{
@Autowired
DocumentRepository repo;
	@Override
	public Document saveDocument(Document document) {
		
		return repo.save(document);
	}
	@Override
	public List<Document> getUserDocumentByName(String name) {
		log.info(name);
	return repo.findByName(name);
	}
	
}
