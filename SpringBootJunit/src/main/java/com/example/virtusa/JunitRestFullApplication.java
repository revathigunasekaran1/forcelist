package com.example.virtusa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class JunitRestFullApplication {

	public static void main(String[] args) {
		SpringApplication.run(JunitRestFullApplication.class, args);
	}

}
