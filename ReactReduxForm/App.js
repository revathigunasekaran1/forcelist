import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
import ProductDetails from './containers/ProductDetails';
import SelectedProductDetails from './containers/SelectedProductDetails'
function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/" component={ProductDetails}/>
          <Route path="/product/:id" component={SelectedProductDetails}/>
        </Switch>
        </Router>
    </div>
  )}
export default App;
