import React, { useEffect } from "react";
import axios from "axios";
import {Link} from 'react-router-dom' 
import { setProducts } from "../redux/actions/productAction";
import { useDispatch, useSelector } from "react-redux";
import { Router } from "react-router-dom";
const ProductDetails = () => {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.allProducts.products);
    const fetchAllProducts = async () => {
    const response = await axios
      .get("https://fakestoreapi.com/products")
      .catch((err) => {
        console.log(err);
      });
    dispatch(setProducts(response.data));
  };

  useEffect(() => {
    fetchAllProducts();
  }, []);
const loadData=products.map((product)=>
{
    const{id,title,image,price,category}=product;
    return(
        <div>
            <Link to={`/product/${id}`}>
            <div>
                <img src={image} width="100" height="100"/>
            </div>
            </Link>
            <div>
                <div>{id}</div>
                <div>{title}</div>
                <div>{price}</div>
                <div>{category}</div>
            </div>
           
        </div>
    );
});
return  <>{loadData}</>;
}
export default ProductDetails;
