import React from 'react';
import App from './App'
import {createStore} from 'redux'
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import reducers from './redux/reducers/index';
const store=createStore(reducers);
 ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
    <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'));


