package com.bank.service;

import java.util.List;

import com.bank.dto.EmpDeptDto;
import com.bank.exception.UserNotFoundException;
import com.bank.model.Register;

public interface RegisterServiceIntf {

	void saveUser(Register user);
	void savePartialData(Register user);
	List<Register> getAllUsers();

	Register getById(int id) throws UserNotFoundException;

	Register getByName(String name);
	EmpDeptDto getByAllEmployees();

}
