package com.bank.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bank.dao.EmployeeDaoIntf;
import com.bank.dao.RegisterDaoIntf;
import com.bank.dto.EmpDeptDto;
import com.bank.exception.UserNotFoundException;
import com.bank.model.Register;

@Service
@Transactional
public class RegisterServiceImpl implements RegisterServiceIntf {

	@Autowired
	RegisterDaoIntf dao;
	
	@Autowired
	EmployeeDaoIntf repo;

	@Override
	public void saveUser(Register reg) {
		dao.save(reg);

	}

	@Override
	public List<Register> getAllUsers() {

		return dao.findAll();
	}

	@Override
	public Register getById(int id) throws UserNotFoundException {
		Optional<Register> op;
		op = dao.findById(id);
		op.orElseThrow(() -> new UserNotFoundException("UserId Not Found"));
		return op.get();

	}

	@Override
	public Register getByName(String name) {
		return dao.readByName(name);
	}

	@Override
	public void savePartialData(Register user) {
		dao.savePartialData(user.getAge());
		
	}

	@Override
	public EmpDeptDto getByAllEmployees() {
		EmpDeptDto emp=repo.getByAllEmployees();
				return emp;
	}
}


//op = dao.findById(id);
//if (op.isPresent())
//	rs = op.get();
//
//else
//	throw new UserNotFoundException("UserId Not Found");
