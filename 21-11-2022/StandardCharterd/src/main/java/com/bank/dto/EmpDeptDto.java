package com.bank.dto;

public class EmpDeptDto {
private int empid;
private String empname;
private int dept_id;
private String dept_name;

public EmpDeptDto(int empid, String empname, int dept_id, String dept_name) {
	super();
	this.empid = empid;
	this.empname = empname;
	this.dept_id = dept_id;
	this.dept_name = dept_name;
}
public int getEmpid() {
	return empid;
}
public void setEmpid(int empid) {
	this.empid = empid;
}
public String getEmpname() {
	return empname;
}
public void setEmpname(String empname) {
	this.empname = empname;
}
public int getDept_id() {
	return dept_id;
}
public void setDept_id(int dept_id) {
	this.dept_id = dept_id;
}
public String getDept_name() {
	return dept_name;
}
public void setDept_name(String dept_name) {
	this.dept_name = dept_name;
}

}
