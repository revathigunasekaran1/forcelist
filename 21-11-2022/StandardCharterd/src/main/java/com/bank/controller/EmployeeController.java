package com.bank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dto.EmpDeptDto;
import com.bank.service.RegisterServiceIntf;

@RestController
public class EmployeeController {
	@Autowired
	RegisterServiceIntf service;
@GetMapping("/fetchByEmployeeId")
public EmpDeptDto fetchByEmployeeId()
{	
	EmpDeptDto emp=service.getByAllEmployees();
	System.out.println(emp);
	return emp;
}
{
	
}
}
