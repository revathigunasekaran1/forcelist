package com.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bank.exception.UserNotFoundException;
import com.bank.model.Register;
import com.bank.service.RegisterServiceIntf;

@RestController
public class RegisterController {

	@Autowired
	RegisterServiceIntf service;

	@PostMapping("/addUser")
	public Register addUser(@RequestBody Register reg) {
		service.saveUser(reg);
		return reg;
	}
	@PostMapping("/addPartialUserData")
	public Register addPartialUserData(@RequestBody Register reg) {
		service.savePartialData(reg);
		return reg;
	}

	@GetMapping("/listUsers")
	public List<Register> listUsers() {
		List<Register> ls = service.getAllUsers();
		return ls;

	}

	@GetMapping("/findByName/{name}")
	public ResponseEntity<Register> findByName(@PathVariable("name") String name) {
		Register ls = null;
		ls = service.getByName(name);
		return new ResponseEntity<Register>(ls, HttpStatus.OK);
	}

	@GetMapping("/findById/{id}")
	// https://www.javainterviewpoint.com/spring-boot-exception-handling/
	public ResponseEntity<Register> listUsers(@PathVariable("id") int id) throws UserNotFoundException {
		Register ls = null;
		ls = service.getById(id);
		return new ResponseEntity<Register>(ls, HttpStatus.OK);
	}

}
