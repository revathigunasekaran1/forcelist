package com.bank.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.model.Register;

@Repository
@Transactional
public interface RegisterDaoIntf extends JpaRepository<Register, Integer> {
	// @Query(value = "select * from register where name=:name",nativeQuery = true)
	Register readByName(String name);// select * from tablename where name=?
//findby
	// readby
	@Modifying
	@Query(value = "insert into register(age) values(:age)", nativeQuery = true)
	void savePartialData(int age);

}
