package com.bank.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.dto.EmpDeptDto;
import com.bank.model.Employee;

@Repository
public interface EmployeeDaoIntf extends JpaRepository<Employee, Integer>{


	@Query("SELECT new com.bank.dto.EmpDeptDto(e.id,e.name, d.deptId, d.deptName) "
			+ "FROM Department d ,Employee e")
	EmpDeptDto getByAllEmployees();
	

}
