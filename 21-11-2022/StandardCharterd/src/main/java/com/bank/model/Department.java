package com.bank.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_depart")
public class Department {
	 @OneToOne(cascade =CascadeType.ALL)
	    @JoinColumn(name = "emp_id")
	    private Employee emp;
public int getDeptId() {
		return deptId;
	}
	public Department() {
	super();
	// TODO Auto-generated constructor stub
}
	public Department(int deptId, String deptName) {
	super();
	this.deptId = deptId;
	this.deptName = deptName;
}
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
private int deptId;
private String deptName;

}
