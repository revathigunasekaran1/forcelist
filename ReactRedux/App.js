import logo from './logo.svg';
import './App.css';
import {increment,decrement} from './store/action/action'
import {useDispatch,useSelector} from 'react-redux'

function App() {
 
  const dispatch=useDispatch();
  const counter=useSelector((state)=>state.counter);
    return (
    
    <div>
      <h1>{counter}</h1>
      <button onClick={()=>dispatch(increment())}>Increment</button>
      <button onClick={()=>dispatch(decrement())}>Decrement</button>
    </div>
   
  );
}

export default App;
