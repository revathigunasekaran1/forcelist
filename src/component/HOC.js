import React, { Component } from 'react'
const UpdatedComponent = (Child) => {
    class HOC extends Component {
        constructor(props) {
            super(props)

            this.state = {
                count: 0
            }
        }
        incrementCount = () => {
            this.setState({ count: this.state.count + 1 });
        }
        render() {
        
            return <Child count={this.state.count} incrementCount={this.incrementCount} />

        }
    }
    return HOC
}
export default UpdatedComponent