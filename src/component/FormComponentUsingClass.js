import React, { Component } from "react";

export class FormComponentUsingClass extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
    };
  }
  formValidation() {
    console.log('Hello')
    let nameError = "";
    let passwordError=""
    if (!this.state.username) nameError = "Username is required";
    if (!this.state.password) passwordError = "Password is required";
    console.log(nameError)
    if(nameError ||  passwordError)
    {
      this.setState({nameError,passwordError})
     
        return false;
    }
    
    return true;

  }
  submit = (e) => {
    e.preventDefault();
    if (this.formValidation()) {
      console.log("username",this.state.username);
      console.log("password",this.state.password);
      this.setState({nameError:"",passwordError:"",username:"",password:""})
    }
  };
  render() {
    return (
      <div>
        <form onSubmit={this.submit}>
          Username:
          <input
            type="text"
            name="username"
            value={this.state.username}
            onChange={(e) => this.setState({ username: e.target.value })}/>
            <span className="text-danger">{this.state.nameError}</span>
         
          <br />
          Password:
          <input
            type="text"
            name="password"
            value={this.state.password}
            onChange={(e) => this.setState({ password: e.target.value })}
          />
           <span className="text-danger">{this.state.passwordError}</span>
          <br />
          <button type="submit">Submit</button>
        </form>
      </div>
    );
  }
}

export default FormComponentUsingClass;
