import React, { Component } from "react";

export class UnControlledComponent1 extends Component {
  constructor(props) {
    super(props);
    this.username = React.createRef();
  }
  submit = (e) => {
    e.preventDefault();
    console.log(this.username.current.value);
  };
  render() {
    return (
      <div>
        <h1>Uncontrolled Component</h1>
        <form onSubmit={this.submit}>
          <input type="text" placeholder="enter username" ref={this.username} />
          <br />
          <button type="submit">Click Me</button>
        </form>
      </div>
    );
  }
}

export default UnControlledComponent1;
