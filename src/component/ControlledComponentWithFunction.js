import React, { useState } from "react";

function ControlledComponentWithFunction() {
  let [val, setval] = useState("revathi");
  return (
    <div>
      {val}
      <input type="text" onChange={(e) => setval(e.target.value)} />
    </div>
  );
}

export default ControlledComponentWithFunction;
