import { Component } from "react";
export default class Clock extends Component {
    constructor(props) {
      super(props);
      this.state = {date: new Date()};
    }
  
    render() {
      return (
        <div>
          <h1>Hello, world!</h1>
          <h2>It is {this.state.date.getFullYear()}.</h2>
        </div>
      );
    }
  }