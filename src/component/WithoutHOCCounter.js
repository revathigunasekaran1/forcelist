import React, { Component } from 'react'
import UpdatedComponent from './HOC'

class WithoutHOCCounter extends Component {
 
    render() {
       
        let {count,incrementCount}=this.props;
        console.log(count);
        return (
            <div>
                <h1>{count}</h1>
                <button onClick={incrementCount}>Click Me</button>
                {/* <button onClick={() => this.setState({ count: this.state.count + 1 })}>Click Me</button> */}
            </div>
        )
    }
}
export default UpdatedComponent(WithoutHOCCounter)
