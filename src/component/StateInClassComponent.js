import React, { Component } from 'react'

export default class StateInClassComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            count: 0
        }
    }
    //this.incrementCount=this.incrementCount.bind(this);

    incrementCount=()=>
    {
       this.setState({count:this.state.count+1});
    }

    render() {
        return (
            <div>
                <h1>{this.state.count}</h1>
                <button onClick={this.incrementCount}>Click Me</button>
                {/* <button onClick={() => this.setState({ count: this.state.count + 1 })}>Click Me</button> */}
            </div>
        )
    }
}
