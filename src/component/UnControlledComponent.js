import React, { Component } from 'react'
//https://blog.logrocket.com/react-create-ref-guide/#:~:text=Using%20React.createRef%20Starting%20from%20React%2016.3%2C%20the%20React,and%20assign%20the%20resulting%20ref%20to%20an%20element.
//https://blog.logrocket.com/react-create-ref-guide/#creating-refs-react
export default class UnControlledComponent extends Component {
    constructor(props) {
        super(props)
      this.name=React.createRef();
    }
    submit(e)
    {
        e.preventDefault();
        console.log(this.name.current.value);

    }
  render() {
    return (
      <div>
        <h1>Uncontrolled component</h1>
        <form onSubmit={(e)=>{this.submit(e)}}>
            <input type="text" ref={this.name}/><br/>
            <button type="submit">submit</button>
        </form>
      </div>
    )
  }
}
