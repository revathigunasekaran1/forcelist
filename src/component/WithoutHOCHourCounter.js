import React, { Component } from 'react'
import UpdatedComponent from './HOC'
class WithoutHOCHourCounter extends Component {

      render() {
        let {count,incrementCount}=this.props;
        return <h2 onMouseOver={incrementCount}>Hovered {count} times</h2>
    }
}
export default UpdatedComponent(WithoutHOCHourCounter)