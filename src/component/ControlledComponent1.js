import React, { Component } from 'react'

export class ControlledComponent1 extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         val:'revathi1'
      }
    }
  render() {
    return (
      <div>
        {this.state.val}
        <input type="text" onChange={(e)=>this.setState({val:e.target.value})}  />
        
        
      </div>
    )
  }
}

export default ControlledComponent1