import { useState } from 'react'

export default function StateInFunctionComponent() {
    let [count, setcount] = useState(0);
    let increment = () => {
        setcount(count + 1)
    }
    return (
        <div>
            <b>{count}</b>
            <button onClick={increment}>Click Me</button>
        </div>
    )
}
