import React, { Component } from 'react'

export default class HourCounter extends Component {
    constructor(props) {
        super(props)

        this.state = {
            count: 0
        }
    }
    //this.incrementCount=this.incrementCount.bind(this);

    incrementCount=()=>
    {
       this.setState({count:this.state.count+1});
    }

    render() {
        return <h2 onMouseOver={this.incrementCount}>Hovered {this.state.count} times</h2>
    }
}
